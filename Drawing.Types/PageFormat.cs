// Copyright (c) 2012 Виталий Сайдин, Ярослав Горин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Yaroslav Gorin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using Sml.Geometry.Data;

namespace Drawing.Types
{
	public class PageFormat
	{
		private PageFormatType	_formatType			= PageFormatType.Custom;
		private PageOrientation	_orientationType	= PageOrientation.Portrait;
		private double _width;
		private double _height;


		public PageFormat() { }

		public PageFormat(PageFormatType formatType)
		{
			ChangeFormatType(formatType);
		}

		public PageFormat(PageFormatType formatType, PageOrientation orient)
		{
			ChangeFormatType(formatType);
			ChangeOrientation(orient);
			_formatType = formatType;
		}

		private void ChangeOrientation(PageOrientation value)
		{
			if (_orientationType != value)
			{
				double temp = _width;
				_width = _height;
				_height = temp;
			}
			_orientationType = value;
		}

		private void ChangeFormatType(PageFormatType value)
		{
			if (_formatType != value)
			{
				PageSize ps = PageSize.Create(value);
				_width = ps.Width;
				_height = ps.Height;
				_formatType = value;
			}
		}

		/// <summary>
		/// Свойство - форматка листа
		/// </summary>
		public PageFormatType FormatType
		{
			get { return _formatType; }
			set { ChangeFormatType(value); }
		}

		/// <summary>
		/// Свойство - ориентация листа
		/// </summary>
		public PageOrientation OrientationType
		{
			get { return _orientationType; }
			set { ChangeOrientation(value); }
		}

		/// <summary>
		/// Ширина листа
		/// </summary>
		public double Width
		{
			get { return _width; }
			//set { _width = value; }
		}

		/// <summary>
		/// Высота листа
		/// </summary>
		public double Height
		{
			get { return _height; }
			//set { _height = value; }
		}

		public SmlRect2D GetRectangle()
		{
			return new SmlRect2D(new SmlPoint2D(), _width, _height);
		}

		public SmlRect2D GetRectangle(SmlPoint2D basePoint)
		{
			return new SmlRect2D(basePoint, _width, _height);
		}
	}
}
