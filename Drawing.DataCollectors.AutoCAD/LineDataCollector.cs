// Copyright (c) 2012 Виталий Сайдин, Ярослав Горин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Yaroslav Gorin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using MAcadFramework.Aggregates;
using MAcadFramework.Routines;
using Sml.AutoCAD.Bridge;
using Sml.Geometry.Data;

namespace Drawing.DataCollectors.AutoCAD
{
    public class LineDataCollector : IDataCollector<SmlSegment2D>
    {
        private delegate List<SmlSegment2D> LinesGetterHandler(List<ObjectId> lines);

        private readonly Dictionary<Type, LinesGetterHandler> _tableLinesGetter =
            new Dictionary<Type, LinesGetterHandler>(10);

        public LineDataCollector()
        {
            _tableLinesGetter.Add(typeof (Line), GetLineFromLines);
            _tableLinesGetter.Add(typeof (Polyline), GetLineFromPolylines);
            _tableLinesGetter.Add(typeof (Polyline2d), GetLineFromPolylines2D);
            _tableLinesGetter.Add(typeof (BlockReference), GetLineFromBlocks);
            _tableLinesGetter.Add(typeof (ProxyEntity), GetLineFromProxyEntity);
        }

        #region GetFromSpaces

        /// <summary>
        /// Получить линии с пространства модели.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SmlSegment2D> GetDataFromModelSpace()
        {
            List<SmlSegment2D> acadLine = new List<SmlSegment2D>(10);
            foreach (Type type in _tableLinesGetter.Keys)
            {
                List<ObjectId> objectIds = DBRoutine.ObjectGetter.FromModelSpace(type);
                if (objectIds.Count == 0) continue;
                acadLine.AddRange(_tableLinesGetter[type](objectIds));
            }
            return acadLine;
        }

        /// <summary>
        /// Gets the line from space.
        /// </summary>
        /// <param name="spaceName">Name of the space.</param>
        /// <param name="layer">The layer.</param>
        /// <returns></returns>
        public IEnumerable<SmlSegment2D> GetDataFromSpace(string spaceName, string layer)
        {
            List<SmlSegment2D> acadLine = new List<SmlSegment2D>(10);
            foreach (Type type in _tableLinesGetter.Keys)
            {
                List<ObjectId> objectIds = DBRoutine.ObjectGetter.FromSpace(spaceName, layer, type);
                if (objectIds.Count == 0) continue;
                acadLine.AddRange(_tableLinesGetter[type](objectIds));
            }
            return acadLine;
        }

        #endregion

        /// <summary>
        /// Gets the line from lines.
        /// </summary>
        /// <param name="lines">The lines.</param>
        /// <returns></returns>
        private List<SmlSegment2D> GetLineFromLines(List<ObjectId> lines)
        {
            List<SmlSegment2D> acadLine = new List<SmlSegment2D>(10);

            foreach (ObjectId id in lines)
            {
                Line l = EntityProvider.GetObjectPointer<Line>(id);
                acadLine.Add(Bridge.ToSmlSegment2D(l));
            }

            return acadLine;
        }

        /// <summary>
        /// Gets the line from polylines.
        /// </summary>
        /// <param name="polylines">The polylines.</param>
        /// <returns></returns>
        private List<SmlSegment2D> GetLineFromPolylines(List<ObjectId> polylines)
        {
            List<SmlSegment2D> acadLine = new List<SmlSegment2D>(10);
            foreach (ObjectId id in polylines)
            {
                Polyline pl = EntityProvider.GetObjectPointer<Polyline>(id);
                if (pl.HasBulges) continue;
                acadLine.AddRange(ConvertPolylineToSegments(pl));
            }
            return acadLine;
        }

        /// <summary>
        /// Gets the line from polylines2D.
        /// </summary>
        /// <param name="polylines">The polylines.</param>
        /// <returns></returns>
        private List<SmlSegment2D> GetLineFromPolylines2D(List<ObjectId> polylines)
        {
            List<SmlSegment2D> acadLine = new List<SmlSegment2D>(10);
            foreach (ObjectId id in polylines)
            {
                Polyline2d pl = EntityProvider.GetObjectPointer<Polyline2d>(id);
                if (HasBulges(pl)) continue;
                acadLine.AddRange(ConvertPolyline2dToSegments(pl));
            }

            return acadLine;
        }

        /// <summary>
        /// Gets the line from blocks.
        /// </summary>
        /// <param name="blocks">The blocks.</param>
        /// <returns></returns>
        private List<SmlSegment2D> GetLineFromBlocks(List<ObjectId> blocks)
        {
            List<SmlSegment2D> acadLine = new List<SmlSegment2D>(10);

            foreach (ObjectId id in blocks)
            {
                BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(id);
                acadLine.AddRange(ConvertBlockReferenceToSegments(br));
            }

            return acadLine;
        }

        /// <summary>
        /// Gets the line from proxy entity.
        /// </summary>
        /// <param name="proxys">The proxys.</param>
        /// <returns></returns>
        private List<SmlSegment2D> GetLineFromProxyEntity(List<ObjectId> proxys)
        {
            List<SmlSegment2D> acadLine = new List<SmlSegment2D>(10);
            EditorRoutine.WriteMessage("ProxyEntity пока не \"собираются\" с чертежа!");
            return acadLine;
        }

        /// <summary>
        /// Определяет содержит ли полилиния криволинейные участки.
        /// </summary>
        /// <param name="pl">полилиния.</param>
        /// <returns>
        /// 	<c>true</c> if the specified pl has bulges; otherwise, <c>false</c>.
        /// </returns>
        private static bool HasBulges(Polyline2d pl)
        {
            //if (pl.PolyType == Poly2dType.SimplePoly)
            //    return true;
            foreach (ObjectId id in pl)
            {
                Vertex2d vert = EntityProvider.GetObjectPointerNoException<Vertex2d>(id);
                if (vert == null) continue;
                if (vert.Bulge == 0) return true;
            }
            return false;
        }

        #region Convertors

        /// <summary>
        /// Converts the block reference to segments.
        /// </summary>
        /// <param name="br">The br.</param>
        /// <returns></returns>
        private IEnumerable<SmlSegment2D> ConvertBlockReferenceToSegments(BlockReference br)
        {
            List<SmlSegment2D> segmentList = new List<SmlSegment2D>();
            DBObjectCollection objectCollection = new DBObjectCollection();
            try
            {
                br.Explode(objectCollection);
                foreach (DBObject dbo in objectCollection)
                {
                    if (dbo is Line)
                    {
                        Line line = dbo as Line;
                        segmentList.Add(Bridge.ToSmlSegment2D(line));
                    }

                    else if (dbo is Polyline)
                    {
                        Polyline polyline = dbo as Polyline;
                        segmentList.AddRange(ConvertPolylineToSegments(polyline));
                    }

                    else if (dbo is Polyline2d)
                    {
                        Polyline2d polyline2d = dbo as Polyline2d;
                        segmentList.AddRange(ConvertPolyline2dToSegments(polyline2d));
                    }

                    else if (dbo is BlockReference)
                    {
                        BlockReference bRef = dbo as BlockReference;
                        segmentList.AddRange(ConvertBlockReferenceToSegments(bRef));
                    }
                }
            }
            catch (Exception ex)
            {
                EditorRoutine.WriteMessage("Не удалось взорвать блок " + br.Name + "\n" + ex.Message);
            }
            finally
            {
                SafelyCreater.Dispose(objectCollection);
            }
            return segmentList;
        }

        /// <summary>
        /// Converts the polyline2d to segments.
        /// </summary>
        /// <param name="pl">The pl.</param>
        /// <returns></returns>
        private static IEnumerable<SmlSegment2D> ConvertPolyline2dToSegments(Polyline2d pl)
        {
            List<SmlSegment2D> acadLine = new List<SmlSegment2D>(10);
            List<Vertex2d> vList = new List<Vertex2d>(10);

            foreach (ObjectId id in pl)
            {
                Vertex2d v = EntityProvider.GetObjectPointer<Vertex2d>(id);
                if (v != null) vList.Add(v);
            }

            for (int i = 1; i < vList.Count; i++)
            {
                SmlPoint2D pt1 = new SmlPoint2D(vList[i - 1].Position.X, vList[i - 1].Position.Y);
                SmlPoint2D pt2 = new SmlPoint2D(vList[i].Position.X, vList[i].Position.Y);
                acadLine.Add(new SmlSegment2D(pt1, pt2));
            }

            return acadLine;
        }

        /// <summary>
        /// Converts the polyline to segments.
        /// </summary>
        /// <param name="pl">The pl.</param>
        /// <returns></returns>
        private static IEnumerable<SmlSegment2D> ConvertPolylineToSegments(Polyline pl)
        {
            List<SmlSegment2D> acadLine = new List<SmlSegment2D>(10);
            for (int i = 1; i < pl.NumberOfVertices; i++)
            {
                Point2d pt1 = pl.GetPoint2dAt(i - 1);
                Point2d pt2 = pl.GetPoint2dAt(i);
                acadLine.Add(Bridge.ToSmlSegment2D(pt1, pt2));
            }

            if (pl.Closed)
            {
                Point2d pt1 = pl.GetPoint2dAt(0);
                Point2d pt2 = pl.GetPoint2dAt(pl.NumberOfVertices - 1);
                acadLine.Add(Bridge.ToSmlSegment2D(pt1, pt2));
            }

            return acadLine;
        }

        #endregion

    }
}
