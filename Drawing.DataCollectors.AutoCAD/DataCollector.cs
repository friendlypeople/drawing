// Copyright (c) 2012 Виталий Сайдин, Ярослав Горин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Yaroslav Gorin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Drawing.Parts;
using Drawing.Texts;
using MAcadFramework.Routines;
using Sml.Geometry.Data;

namespace Drawing.DataCollectors.AutoCAD
{
    public class DataCollector
    {
        readonly IDataCollector<Text> _textDataCollector = new TextDataCollector();
        readonly IDataCollector<SmlSegment2D> _lineDataCollector = new LineDataCollector();

        /// <summary>
        /// Получить DrawingPart(Представление чертежа) с пространства модели.
        /// </summary>
        /// <returns></returns>
        public DrawingPart GetDrawingPartFromModelSpace()
        {
            return new DrawingPart(_lineDataCollector.GetDataFromModelSpace(), _textDataCollector.GetDataFromModelSpace());
        }

        /// <summary>
        /// Получить DrawingPart(Представление чертежа) с заданного пространства и заданного слоя.
        /// </summary>
        /// <param name="spaceName">Name of the space.</param>
        /// <param name="layer">The layer.</param>
        /// <returns></returns>
        public DrawingPart GetDrawingPartFromSpace(string spaceName, string layer)
        {
            return new DrawingPart(_lineDataCollector.GetDataFromSpace(spaceName, layer), _textDataCollector.GetDataFromSpace(spaceName, layer));
        }

        #region LayoutGetter

        /// <summary>
        ///  Получение существующих лэйоутов
        /// </summary>
        /// <returns>Словарь - BlockTableRecord.Name, Layout</returns>
        public List<Layout> GetExistLayouts()
        {
            using (Transaction tr = DBRoutine.StartTransaction())
            {
                List<Layout> layoutTable = new List<Layout>();
                BlockTable bt = (BlockTable) tr.GetObject(DBRoutine.WellKnownTableId.BlockTableId, OpenMode.ForRead);
                foreach (ObjectId btrId in bt)
                {
                    BlockTableRecord btr = (BlockTableRecord) tr.GetObject(btrId, OpenMode.ForRead);
                    if (btr.IsLayout && btr.Name.ToUpper() != BlockTableRecord.ModelSpace.ToUpper())
                    {
                        Layout layout = (Layout)tr.GetObject(btr.LayoutId, OpenMode.ForRead);
                        layoutTable.Add(layout);
                    }
                }
                return layoutTable;
            }
        }

        #endregion
    }
}
