// Copyright (c) 2012 Виталий Сайдин, Ярослав Горин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Yaroslav Gorin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Drawing.Texts;
using MAcadFramework.Aggregates;
using MAcadFramework.Routines;

namespace Drawing.DataCollectors.AutoCAD
{
    public class TextDataCollector : IDataCollector<Text>
    {
        private delegate List<Text> TextsGetterHandler(List<ObjectId> texts);

        private readonly Dictionary<Type, TextsGetterHandler> _tableTextsGetter =
            new Dictionary<Type, TextsGetterHandler>(10);

        public TextDataCollector()
        {
            _tableTextsGetter.Add(typeof(DBText), GetTextsFromDBTexts);
            _tableTextsGetter.Add(typeof(BlockReference), GetTextFromBlocks);
            _tableTextsGetter.Add(typeof(AttributeDefinition), GetTextsFromAttributeDefinitions);
        }
        
        #region GetFromSpaces

        /// <summary>
        /// Получить надписи с пространства модели.
        /// </summary>
        public IEnumerable<Text> GetDataFromModelSpace()
        {
            List<Text> acadTexts = new List<Text>(10);
            foreach (Type type in _tableTextsGetter.Keys)
            {
                List<ObjectId> objectIds = DBRoutine.ObjectGetter.FromModelSpace(type);
                if (objectIds.Count == 0) continue;
                acadTexts.AddRange(_tableTextsGetter[type](objectIds));
            }
            return acadTexts;
        }

        /// <summary>
        /// Получить надписи с заданного пространства.
        /// </summary>
        public IEnumerable<Text> GetDataFromSpace(string spaceName, string layer)
        {
            List<Text> acadTexts = new List<Text>(10);
            foreach (Type type in _tableTextsGetter.Keys)
            {
                List<ObjectId> objectIds = DBRoutine.ObjectGetter.FromSpace(spaceName, layer, type);
                if (objectIds.Count == 0) continue;
                acadTexts.AddRange(_tableTextsGetter[type](objectIds));
            }
            return acadTexts;
        }

        #endregion

        /// <summary>
        /// Получить список Text'ов из блоков.
        /// </summary>
        /// <param name="blocks">Список ObjectId блоков (BlockReference)</param>
        /// <returns>Список Text'ов</returns>
        private List<Text> GetTextFromBlocks(List<ObjectId> blocks)
        {
            List<Text> textList = new List<Text>(10);

            foreach (ObjectId id in blocks)
            {
                BlockReference br = EntityProvider.GetObjectPointer<BlockReference>(id);
                textList.AddRange(ExtractTextsFromBlockReference(br));
            }

            return textList;
        }

        /// <summary>
        /// Найти тексты (DBText) в составе блока.
        /// </summary>
        private IEnumerable<Text> ExtractTextsFromBlockReference(BlockReference br)
        {
            List<Text> textList = new List<Text>();
            DBObjectCollection objectCollection = new DBObjectCollection();
            try
            {
                br.Explode(objectCollection);
                foreach (DBObject dbo in objectCollection)
                {
                    if (dbo is DBText)
                    {
                        Text item;
                        if (TryConvertDBTextToText(dbo as DBText, out item))
                            textList.Add(item);
                    }
                    else if (dbo is BlockReference)
                    {
                        BlockReference bRef = dbo as BlockReference;
                        textList.AddRange(ExtractTextsFromBlockReference(bRef));
                    }
                }
            }
            catch (Exception ex)
            {
                EditorRoutine.WriteMessage("Не удалось взорвать блок " + br.Name + "\n" + ex.Message);
            }
            finally
            {
                SafelyCreater.Dispose(objectCollection);
            }
            return textList;
        }

        /// <summary>
        /// Получить список Text'ов по списку ObjectId DBText'ов AutoCAD.
        /// </summary>
        /// <param name="dbtexts">Список ObjectId DBText'ов AutoCAD.</param>
        /// <returns>Список Text'ов.</returns>
        private List<Text> GetTextsFromDBTexts(List<ObjectId> dbtexts)
        {
            List<Text> acadTexts = new List<Text>(10);
            foreach (ObjectId id in dbtexts)
            {
                DBText dbText = EntityProvider.GetObjectPointer<DBText>(id);
                Text text;
                if (TryConvertDBTextToText(dbText, out text))
                    acadTexts.Add(text);
            }
            return acadTexts;
        }

        /// <summary>
        /// Преобразовать DBText в Text
        /// </summary>
        /// <param name="dbText">Исходный DBText</param>
        /// <param name="text">Результат</param>
        /// <returns>true, если преобразование успешно; иначе false.</returns>
        private bool TryConvertDBTextToText(DBText dbText, out Text text)
        {
            text = null;
            Text item = TextBridge.ToText(dbText);
            if (item.IsValid)
            {
                text = item;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Получить список Text'ов по списку ObjectId AttributeDefinition'ов AutoCAD.
        /// </summary>
        /// <param name="attDefList">Список ObjectId AttributeDefinition'ов AutoCAD.</param>
        /// <returns>Список Text'ов.</returns>
        private List<Text> GetTextsFromAttributeDefinitions(List<ObjectId> attDefList)
        {
            List<Text> acadTexts = new List<Text>(10);
            foreach (ObjectId id in attDefList)
            {
                AttributeDefinition attDef = EntityProvider.GetObjectPointer<AttributeDefinition>(id);
                Text text;
                if (TryConvertAttributeDefinitionToText(attDef, out text))
                    acadTexts.Add(text);
            }
            return acadTexts;
        }

        /// <summary>
        /// Преобразовать DBText в Text
        /// </summary>
        /// <param name="attDef">Исходный DBText</param>
        /// <param name="text">Результат</param>
        /// <returns>true, если преобразование успешно; иначе false.</returns>
        private bool TryConvertAttributeDefinitionToText(AttributeDefinition attDef, out Text text)
        {
            text = null;
            Text item = TextBridge.ToText(attDef);
            if (item.IsValid)
            {
                text = item;
                return true;
            }
            return false;
        }
    }
}
