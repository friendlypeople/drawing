// Copyright (c) 2012 Виталий Сайдин, Ярослав Горин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Yaroslav Gorin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Sml.Geometry;
using Sml.Geometry.Data;

namespace Drawing.Parts
{
    /// <summary>
    /// Класс реализующий работу по слиянию горизонтальных и вертикальных линий.
    /// </summary>
	public sealed class SegmentComplimentator
    {
        /// <summary>
        /// Критерий горизонтальности сегмента
        /// </summary>
        private const double _horizontalCriteria = 0.01;

        /// <summary>
        /// Начать слияние.
        /// </summary>
        /// <param name="segmentList">Список сегментов, подлежащих слиянию.</param>
        /// <returns>Список слитых сегментов </returns>
		public List<SmlSegment2D> Run(List<SmlSegment2D> segmentList)
		{
			List<SmlSegment2D> retList = new List<SmlSegment2D>(20);
			IEnumerable<List<SmlSegment2D>> horisontalSegmentLists = GetHorisontalSegmentLists(segmentList);
			retList.AddRange(Merge(horisontalSegmentLists));

			IEnumerable<List<SmlSegment2D>> verticalSegmentLists = GetVerticalSegmentLists(segmentList);
			retList.AddRange(Merge(verticalSegmentLists));
			return retList;
		}

		private IEnumerable<SmlSegment2D> Merge(IEnumerable<List<SmlSegment2D>> rows)
		{
			List<SmlSegment2D> list = new List<SmlSegment2D>();
			foreach (List<SmlSegment2D> row in rows)
			{
				list.AddRange(MergeSegments(row));
			}
			return list;
		}

		private IEnumerable<SmlSegment2D> MergeSegments(IEnumerable<SmlSegment2D> row)
		{
			List<SmlSegment2D> list = new List<SmlSegment2D>();
			foreach (SmlSegment2D seg in row)
			{
				bool isNew = true;
				foreach (SmlSegment2D s in list)
				{
					if (CheckSegments(seg, s))
					{
						isNew = false;
						list.Remove(s);
						list.Add(MargeSegments(seg, s));
						break;
					}
				}
				if (isNew) list.Add(seg);
			}
			return list;
		}

		private SmlSegment2D MargeSegments(SmlSegment2D s1, SmlSegment2D s2)
		{
			SmlMinMaxPoints findMinMaxPoint = GeomUtils.FindMinMaxPoint(s1.StartPoint, s1.EndPoint, s2.StartPoint, s2.EndPoint);
			return new SmlSegment2D(findMinMaxPoint.MinPoint, findMinMaxPoint.MaxPoint);
		}

		private bool CheckSegments(SmlSegment2D s1, SmlSegment2D s2)
		{
			if (s1.IsOn(s2.StartPoint) || s1.IsOn(s2.EndPoint))
				return true;
			if (s2.IsOn(s1.StartPoint) || s2.IsOn(s1.EndPoint))
				return true;
			return false;
		}

		private IEnumerable<List<SmlSegment2D>> GetHorisontalSegmentLists(IEnumerable<SmlSegment2D> segments)
		{
			List<List<SmlSegment2D>> ret = new List<List<SmlSegment2D>>();
			foreach (SmlSegment2D segment in segments)
			{
                if (!segment.IsHorizontal(_horizontalCriteria)) continue;
				bool isNewRow = true;
				foreach (List<SmlSegment2D> row in ret)
				{
					SmlSegment2D rowSegment = row[0];
                    if (Math.Abs(rowSegment.StartPoint.Y - segment.StartPoint.Y) < _horizontalCriteria)
					{
						isNewRow = false;
						row.Add(segment);
						break;
					}
				}
				if (isNewRow)
				{
					List<SmlSegment2D> newRow = new List<SmlSegment2D>();
					newRow.Add(segment);
					ret.Add(newRow);
				}
			}
			return ret;
		}

		private IEnumerable<List<SmlSegment2D>> GetVerticalSegmentLists(IEnumerable<SmlSegment2D> segments)
		{
			List<List<SmlSegment2D>> ret = new List<List<SmlSegment2D>>();
			foreach (SmlSegment2D segment in segments)
			{
				if (!segment.IsVertical(0.01)) continue;
				bool isNewRow = true;
				foreach (List<SmlSegment2D> column in ret)
				{
					SmlSegment2D colSegment = column[0];
					if (Math.Abs(colSegment.StartPoint.X - segment.StartPoint.X) < 0.01)
					{
						isNewRow = false;
						column.Add(segment);
						break;
					}
				}
				if (isNewRow)
				{
					List<SmlSegment2D> newColumn = new List<SmlSegment2D>();
					newColumn.Add(segment);
					ret.Add(newColumn);
				}
			}
			return ret;
		}
	}
}
