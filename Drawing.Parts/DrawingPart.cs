// Copyright (c) 2012 Виталий Сайдин, Ярослав Горин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Yaroslav Gorin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections.Generic;
using Drawing.Texts;
using Sml.Geometry;
using Sml.Geometry.Data;

namespace Drawing.Parts
{
	public class DrawingPart
	{
        private List<SmlSegment2D> _segmentList = new List<SmlSegment2D>();
		private List<Text> _textList = new List<Text>();
		private object _tag;

        public List<SmlSegment2D> SegmentList
		{
			get { return _segmentList; }
			set { _segmentList = value; }
		}

		public List<Text> TextList
		{
			get { return _textList; }
			set { _textList = value; }
		}

		public object Tag
		{
			get { return _tag; }
			set { _tag = value; }
		}

		public DrawingPart() { }

        public DrawingPart(IEnumerable<SmlSegment2D> segmentList)
		{
			_segmentList = new List<SmlSegment2D>(segmentList);
		}

		public DrawingPart(IEnumerable<Text> textList)
		{
			_textList = new List<Text>(textList);
		}

		public DrawingPart(IEnumerable<SmlSegment2D> segmentList, IEnumerable<Text> textList)
		{
			_segmentList = new List<SmlSegment2D>(segmentList);
			_textList = new List<Text>(textList);
		}

		public class SegmentComparer : IComparer<SmlSegment2D>
		{
			public int Compare(SmlSegment2D a, SmlSegment2D b)
			{
				double aX = a.StartPoint.X > a.EndPoint.X ? a.EndPoint.X : a.StartPoint.X;
				double bX = b.StartPoint.X > b.EndPoint.X ? b.EndPoint.X : b.StartPoint.X;

				return aX.CompareTo(bX);
			}
		}

		/// <summary>
		/// Добавление текста
		/// </summary>
		/// <param name="text">Текст.</param>
		public void Add(Text text)
		{
			_textList.Add(text);
		}

		/// <summary>
		/// Добавление списка текстов
		/// </summary>
		/// <param name="textList">Список текстов.</param>
		public void Add(IEnumerable<Text> textList)
		{
			_textList.AddRange(textList);
		}

		/// <summary>
		/// Добавление сегмента
		/// </summary>
		/// <param name="segment">Сегмент.</param>
		public void Add(SmlSegment2D segment)
		{
			_segmentList.Add(segment);
		}

		/// <summary>
		/// Добавление списка сегмента
		/// </summary>
		/// <param name="segmentList">Список сегментов.</param>
		public void Add(IEnumerable<SmlSegment2D> segmentList)
		{
			_segmentList.AddRange(segmentList);
		}

		/// <summary>
		/// Получить точки с сегментов
		/// </summary>
		/// <returns>Список точек</returns>
		public List<SmlPoint2D> GetPointsFromSegment()
		{
			List<SmlPoint2D> points = new List<SmlPoint2D>(100);
			foreach (SmlSegment2D segment in SegmentList)
			{
				points.Add(new SmlPoint2D(segment.StartPoint));
				points.Add(new SmlPoint2D(segment.EndPoint));
			}
			return points;
		}

		/// <summary>
		/// Создание описываемого прямоугольника вокруг DrawingPart
		/// </summary>
		public SmlBoundaryRect2D CreateBoundaryRectangle()
		{
			List<SmlPoint2D> pointsFromSegment = GetPointsFromSegment();
			return GeomUtils.CreateBoundaryRectangle(pointsFromSegment);
		}

		#region SelectTexts methods

		/// <summary>
		/// Получить тексты, используя прямоугольное выделение.
		/// </summary>
		/// <param name="rect">Прямоугольник.</param>
		/// <param name="type">Опции</param>
		/// <returns></returns>
		public List<Text> SelectTextsByRectangle(SmlBoundaryRect2D rect, PartSelectionOptions type)
		{
			List<Text> textList = new List<Text>();

			if (type == PartSelectionOptions.InternalEntityOnly)
			{
				foreach (Text text in TextList)
				{
					if (IsPointInsideRectangle(text.OutlineRect.BasePoint, rect.BasePoint, rect.OppositePoint) && IsPointInsideRectangle(text.OutlineRect.OppositePoint, rect.BasePoint, rect.OppositePoint))
						textList.Add(text);
				}
			}
			else if (type == PartSelectionOptions.InternalAndCrossingEntity)
			{
				foreach (Text text in TextList)
				{
					if (IsPointInsideRectangle(text.OutlineRect.BasePoint, rect.BasePoint, rect.OppositePoint) || IsPointInsideRectangle(text.OutlineRect.OppositePoint, rect.BasePoint, rect.OppositePoint))
						textList.Add(text);
					else if (rect.HasIntersect(new SmlSegment2D(text.OutlineRect.BasePoint, text.OutlineRect.OppositePoint)))
						textList.Add(text);
				}
			}

			return textList;
		}

		/// <summary>
		/// Получить тексты, пересечённые отрезком.
		/// </summary>
		/// <param name="pt1">Левая нижняя точка прямоугольника.</param>
		/// <param name="pt2">Правая верхняя точка прямоугольника.</param>
		/// <param name="pso">Опции</param>
		/// <returns></returns>
		public List<Text> SelectTextsByRectangle(SmlPoint2D pt1, SmlPoint2D pt2, PartSelectionOptions pso)
		{
		    return SmlRect2D.CanCreate(pt1, pt2) ? SelectTextsByRectangle(new SmlBoundaryRect2D(pt1, pt2), pso) : SelectTextsByCrossingLine(pt1, pt2);
		}

        /// <summary>
        /// Получить сегменты.
        /// </summary>
        /// <param name="pt1">Левая нижняя точка прямоугольника.</param>
        /// <param name="pt2">Правая верхняя точка прямоугольника.</param>
        /// <param name="pso">Опции</param>
        /// <returns></returns>
        public List<SmlSegment2D> SelectSegmentsByRectangle(SmlPoint2D pt1, SmlPoint2D pt2, PartSelectionOptions pso)
        {
            return SmlRect2D.CanCreate(pt1, pt2) ? SelectSegmentsByRectangle(new SmlBoundaryRect2D(pt1, pt2), pso) : SelectSegmentsByCrossingLine(pt1, pt2);
        }

        /// <summary>
        /// Получить сегменты, используя прямоугольное выделение.
        /// </summary>
        /// <param name="rect">Прямоугольник.</param>
        /// <param name="type">Опции</param>
        /// <returns></returns>
        public List<SmlSegment2D> SelectSegmentsByRectangle(SmlBoundaryRect2D rect, PartSelectionOptions type)
        {
            List<SmlSegment2D> segmentList = new List<SmlSegment2D>();

            if (type == PartSelectionOptions.InternalEntityOnly)
            {
                foreach (SmlSegment2D segment in SegmentList)
                {
                    if (IsPointInsideRectangle(segment.StartPoint, rect.BasePoint, rect.OppositePoint) && IsPointInsideRectangle(segment.EndPoint, rect.BasePoint, rect.OppositePoint))
                        segmentList.Add(segment);
                }
            }
            else if (type == PartSelectionOptions.InternalAndCrossingEntity)
            {
                foreach (SmlSegment2D segment in SegmentList)
                {
                    if (IsPointInsideRectangle(segment.StartPoint, rect.BasePoint, rect.OppositePoint) || IsPointInsideRectangle(segment.EndPoint, rect.BasePoint, rect.OppositePoint))
                        segmentList.Add(segment);
                    else if (rect.HasIntersect(new SmlSegment2D(segment.StartPoint, segment.EndPoint)))
                        segmentList.Add(segment);
                }
            }

            return segmentList;
        }

	    /// <summary>
		/// Получить тексты, пересечённые отрезком.
		/// </summary>
		/// <param name="pt1">1ая точка секущего отрезка.</param>
		/// <param name="pt2">2ая точка секущего отрезка.</param>
		/// <returns></returns>
		public List<Text> SelectTextsByCrossingLine(SmlPoint2D pt1, SmlPoint2D pt2)
		{
			List<Text> textList = new List<Text>();
			SmlSegment2D seg = new SmlSegment2D(pt1, pt2);

			foreach (Text text in TextList)
			{
				if (text.OutlineRect.HasIntersect(seg))
					textList.Add(text);
			}

			return textList;
		}

		/// <summary>
		/// Получить тексты, пересечённые отрезком.
		/// </summary>
		/// <param name="segment">Секущий отрезок.</param>
		/// <returns></returns>
		public List<Text> SelectTextsByCrossingLine(SmlSegment2D segment)
		{
			return SelectTextsByCrossingLine(segment.StartPoint, segment.EndPoint);
		}

		/// <summary>
		/// Получить тексты, пересечённые отрезком.
		/// </summary>
		/// <param name="pt1">1ая точка секущего отрезка.</param>
		/// <param name="pt2">2ая точка секущего отрезка.</param>
		/// <param name="comparer">Копаратор для сортировки.</param>
		/// <returns></returns>
		public List<Text> SelectTextsByCrossingLine(SmlPoint2D pt1, SmlPoint2D pt2, IComparer<Text> comparer)
		{
			List<Text> textList = SelectTextsByCrossingLine(pt1, pt2);
			textList.Sort(comparer);

			return textList;
		}

		#endregion

		#region SelectSegments methods

		/// <summary>
		/// Получить сегменты.
		/// </summary>
		/// <param name="segment">Секущий отрезок.</param>
		/// <returns></returns>
		public List<SmlSegment2D> SelectSegmentsByCrossingLine(SmlSegment2D segment)
		{
			return SelectSegmentsByCrossingLine(segment.StartPoint, segment.EndPoint);
		}

		/// <summary>
		/// Получить сегменты.
		/// </summary>
		/// <param name="pt1">1ая точка секущего отрезка.</param>
		/// <param name="pt2">2ая точка секущего отрезка.</param>
		/// <returns></returns>
		public List<SmlSegment2D> SelectSegmentsByCrossingLine(SmlPoint2D pt1, SmlPoint2D pt2)
		{
			List<SmlSegment2D> segmentList = new List<SmlSegment2D>();
			SmlSegment2D seg = new SmlSegment2D(pt1, pt2);
			SmlPoint2D point = new SmlPoint2D();

			SmlMinMaxPoints minMaxPoints = GeomUtils.FindMinMaxPoint(seg.StartPoint, seg.EndPoint);
			SmlPoint2D maxPoint = minMaxPoints.MaxPoint;
			SmlPoint2D minPoint = minMaxPoints.MinPoint;

			foreach (SmlSegment2D s in SegmentList)
			{
				if (maxPoint.Y < s.StartPoint.Y && maxPoint.Y < s.EndPoint.Y) continue;
				if (maxPoint.X < s.StartPoint.X && maxPoint.X < s.EndPoint.X) continue;
				if (minPoint.Y > s.StartPoint.Y && minPoint.Y > s.EndPoint.Y) continue;
				if (minPoint.X > s.StartPoint.X && minPoint.X > s.EndPoint.X) continue;

				if (seg.CrossPoint(s, ref point))
					segmentList.Add(s);
			}

			return segmentList;
		}

		/// <summary>
		/// Получить сегменты.
		/// </summary>
		/// <param name="pt1">1ая точка секущего отрезка.</param>
		/// <param name="pt2">2ая точка секущего отрезка.</param>
		/// <param name="comparer">Компаратор для сортировки.</param>
		/// <returns></returns>
		public List<SmlSegment2D> SelectSegmentsByCrossingLine(SmlPoint2D pt1, SmlPoint2D pt2, IComparer<SmlSegment2D> comparer)
		{
			List<SmlSegment2D> segmentList = SelectSegmentsByCrossingLine(pt1, pt2);
			segmentList.Sort(comparer);

			return segmentList;
		}

		#endregion

		#region Slice and subtract methods

		/// <summary>
		/// Разбить DrawingPart разделительной линией на 2 части.
		/// </summary>
		/// <param name="separateSegm">Разделительный сегмент.</param>
		/// <returns></returns>
		public DrawingPart[] Slice(SmlSegment2D separateSegm)
		{
			DrawingPart[] drawings = new DrawingPart[2];

			List<SmlSegment2D> leftPartSegments;
			List<SmlSegment2D> rightPartSegments;
			SliceSegments(separateSegm, out leftPartSegments, out rightPartSegments);

			List<Text> leftPartTexts;
			List<Text> rightPartTexts;
			SliceTexts(separateSegm, out leftPartTexts, out rightPartTexts);

			drawings[0] = new DrawingPart(leftPartSegments, leftPartTexts);
			drawings[1] = new DrawingPart(rightPartSegments, rightPartTexts);

			return drawings;
		}

		/// <summary>
		/// Разбить DrawingPart разделительной линией на 2 части.
		/// </summary>
		/// <param name="point">Точка MGeometryFramework.</param>
		/// <param name="direction">Направление разделительной линии.</param>
		/// <returns></returns>
		public DrawingPart[] Slice(SmlPoint2D point, Direction direction)
		{
			SmlSegment2D separateSegm = new SmlSegment2D();

			if (direction == Direction.Vertical)
				separateSegm.Set(point, new SmlPoint2D(point.X, point.Y + 1));
			else
				separateSegm.Set(point, new SmlPoint2D(point.X + 1, point.Y));

			return Slice(separateSegm);
		}

		/// <summary>
		/// Разбить DrawingPart разделительной линией на 2 части.
		/// </summary>
		/// <param name="coordinate">Координата X или Y.</param>
		/// <param name="direction">Направление.</param>
		/// <returns></returns>
		public DrawingPart[] Slice(double coordinate, Direction direction)
		{
			SmlPoint2D pt1;
			if (direction == Direction.Horizontal)
				pt1 = new SmlPoint2D(0, coordinate);

			else if (direction == Direction.Vertical)
				pt1 = new SmlPoint2D(coordinate, 0);

			else
				return new DrawingPart[0];

			return Slice(pt1, direction);
		}

		/// <summary>
		/// Разбить тексты DrawingPart разделительной линией на 2 части.
		/// </summary>
		/// <param name="separateSegm">Разделительный сегмент.</param>
		/// <param name="leftPartTexts">Тексты левой части.</param>
		/// <param name="rightPartTexts">Тексты правой части.</param>
		private void SliceTexts(SmlSegment2D separateSegm, out List<Text> leftPartTexts, out List<Text> rightPartTexts)
		{
			leftPartTexts = new List<Text>();
			rightPartTexts = new List<Text>();

			foreach (Text text in TextList)
			{
				Location basePointLocation = separateSegm.Locate(text.OutlineRect.BasePoint);
				Location oppositePointLocation = separateSegm.Locate(text.OutlineRect.OppositePoint);
				if (basePointLocation == oppositePointLocation)
				{
					if (basePointLocation == Location.Left)
						leftPartTexts.Add(text);
					else if (basePointLocation == Location.Right)
						rightPartTexts.Add(text);
					else
					{
						leftPartTexts.Add(text);
						rightPartTexts.Add(text);
					}
				}
				else
				{
					if (basePointLocation == Location.Left && oppositePointLocation != Location.Right ||
						oppositePointLocation == Location.Left && basePointLocation != Location.Right)
						leftPartTexts.Add(text);
					else if (basePointLocation == Location.Right && oppositePointLocation != Location.Left ||
							 oppositePointLocation == Location.Right && basePointLocation != Location.Left)
						rightPartTexts.Add(text);
					else
					{
						leftPartTexts.Add(text);
						rightPartTexts.Add(text);
					}
				}

			}
		}

		/// <summary>
		///  Разбить сегменты DrawingPart разделительной линией на 2 части.  
		/// </summary>
		/// <param name="separateSegm">Разделительный сегмент.</param>
		/// <param name="leftPartSegments">Сегменты левой части.</param>
		/// <param name="rightPartSegments">Сегменты правой части.</param>
		private void SliceSegments(SmlSegment2D separateSegm, out List<SmlSegment2D> leftPartSegments, out List<SmlSegment2D> rightPartSegments)
		{
			leftPartSegments = new List<SmlSegment2D>();
			rightPartSegments = new List<SmlSegment2D>();

			foreach (SmlSegment2D segment in SegmentList)
			{
				Location startPointLocation = separateSegm.Locate(segment.StartPoint);
				Location endPointLocation = separateSegm.Locate(segment.EndPoint);
				if (startPointLocation == endPointLocation)
				{
					if (startPointLocation == Location.Left)
						leftPartSegments.Add(segment);
					else if (startPointLocation == Location.Right)
						rightPartSegments.Add(segment);
					else
					{
						leftPartSegments.Add(segment);
						rightPartSegments.Add(segment);
					}
				}
				else
				{
					if (startPointLocation == Location.Left && endPointLocation != Location.Right ||
						endPointLocation == Location.Left && startPointLocation != Location.Right)
						leftPartSegments.Add(segment);
					else if (startPointLocation == Location.Right && endPointLocation != Location.Left ||
							 endPointLocation == Location.Right && startPointLocation != Location.Left)
						rightPartSegments.Add(segment);
					else
					{
						leftPartSegments.Add(segment);
						rightPartSegments.Add(segment);
					}
				}
			}
		}

		/// <summary>
		/// Вычесть часть чертежа прямоугольной формы.
		/// </summary>
		/// <param name="boundaryRect">Прямоугольник.</param>
		/// <param name="option">Настройки вычитаяния.</param>
		/// <returns></returns>
		public DrawingPart SubtractPartByRectangle(SmlRect2D boundaryRect, PartSubtractOptions option)
		{
			SmlPoint2D point = new SmlPoint2D(boundaryRect.BasePoint.X + boundaryRect.Width, boundaryRect.BasePoint.Y + boundaryRect.Height);
			return SubtractPartByRectangle(boundaryRect.BasePoint, point, option);
		}

		/// <summary>
		/// Вычесть часть чертежа прямоугольной формы.
		/// </summary>
		/// <param name="pt1">Левая нижняя часть прямоугольника.</param>
		/// <param name="pt2">Правая верхняя часть прямоугольника.</param>
		/// <param name="option">Настройки вычитаяния.</param>
		/// <returns></returns>
		public DrawingPart SubtractPartByRectangle(SmlPoint2D pt1, SmlPoint2D pt2, PartSubtractOptions option)
		{
			List<SmlSegment2D> subtractedSegments;
			List<SmlSegment2D> remainingSegments;
			List<Text> subtractedTexts;
			List<Text> remainingTexts;

			if (option == PartSubtractOptions.InternalSegmentsOnly)
			{
				SubtractInternalSegmentsOnly(pt1, pt2, out subtractedSegments, out remainingSegments);
				SubtractInternalTextsOnly(pt1, pt2, out subtractedTexts, out remainingTexts);
			}
			else if (option == PartSubtractOptions.InternalAndCrossingSegments)
			{
				SubtractInternalAndCrossingSegments(pt1, pt2, out subtractedSegments, out remainingSegments);
				SubtractInternalAndCrossingTextsOnly(pt1, pt2, out subtractedTexts, out remainingTexts);
			}
			else if (option == PartSubtractOptions.InternalAndAddCrossingSegments)
			{
				SubtractInternalAndAddCrossingSegments(pt1, pt2, out subtractedSegments, out remainingSegments);
				SubtractInternalAndAddCrossingTexts(pt1, pt2, out subtractedTexts, out remainingTexts);
			}
			else
			{
				subtractedSegments = new List<SmlSegment2D>();
				remainingSegments = new List<SmlSegment2D>();
				subtractedTexts = new List<Text>();
				remainingTexts = new List<Text>();
			}

			SegmentList = remainingSegments;
			TextList = remainingTexts;

			return new DrawingPart(subtractedSegments, subtractedTexts);
		}

		/// <summary>
		/// Вычесть только внутренние сегменты.
		/// </summary>
		/// <param name="pt1">Левая нижняя часть прямоугольника.</param>
		/// <param name="pt2">Правая верхняя часть прямоугольника.</param>
		/// <param name="subtractedTexts">Вычтенные тексты.</param>
		/// <param name="remainingTexts">Оставшиеся тексты.</param>
		private void SubtractInternalTextsOnly(SmlPoint2D pt1, SmlPoint2D pt2, out List<Text> subtractedTexts, out List<Text> remainingTexts)
		{
			subtractedTexts = new List<Text>();
			remainingTexts = new List<Text>();

			foreach (Text text in TextList)
			{

				if (IsPointInsideRectangle(text.OutlineRect.BasePoint, pt1, pt2) && IsPointInsideRectangle(text.OutlineRect.OppositePoint, pt1, pt2))
					subtractedTexts.Add(text);
				else
					remainingTexts.Add(text);
			}
		}

		/// <summary>
		/// Вычесть только внутренние сегменты.
		/// </summary>
		/// <param name="pt1">Левая нижняя часть прямоугольника.</param>
		/// <param name="pt2">Правая верхняя часть прямоугольника.</param>
		/// <param name="subtractedSegments">Вычтенные сегменты.</param>
		/// <param name="remainingSegments">Оставшиеся сегменты.</param>
		private void SubtractInternalSegmentsOnly(SmlPoint2D pt1, SmlPoint2D pt2, out List<SmlSegment2D> subtractedSegments, out List<SmlSegment2D> remainingSegments)
		{
			subtractedSegments = new List<SmlSegment2D>();
			remainingSegments = new List<SmlSegment2D>();

			foreach (SmlSegment2D segment in SegmentList)
			{
				if (IsPointInsideRectangle(segment.StartPoint, pt1, pt2) && IsPointInsideRectangle(segment.EndPoint, pt1, pt2))
					subtractedSegments.Add(segment);
				else
					remainingSegments.Add(segment);
			}
		}

		/// <summary>
		/// Вычесть внутренние и добавить пересекающие прямоугольник сегменты в списки вычтенных и оставшихся.
		/// </summary>
		/// <param name="pt1">Левая нижняя часть прямоугольника.</param>
		/// <param name="pt2">Правая верхняя часть прямоугольника.</param>
		/// <param name="subtractedTexts">Вычтенные тексты.</param>
		/// <param name="remainingTexts">Оставшиеся тексты.</param>
		private void SubtractInternalAndCrossingTextsOnly(SmlPoint2D pt1, SmlPoint2D pt2, out List<Text> subtractedTexts, out List<Text> remainingTexts)
		{
			subtractedTexts = new List<Text>();
			remainingTexts = new List<Text>();

			List<Text> crossedTexts = new List<Text>();

			foreach (Text text in TextList)
			{
				bool isStartPointInside = IsPointInsideRectangle(text.OutlineRect.BasePoint, pt1, pt2);
				bool isEndPointInside = IsPointInsideRectangle(text.OutlineRect.OppositePoint, pt1, pt2);

				if (isStartPointInside && isEndPointInside)
					subtractedTexts.Add(text);
				else if (isStartPointInside || isEndPointInside)
					crossedTexts.Add(text);
				else
					remainingTexts.Add(text);
			}

			subtractedTexts.AddRange(crossedTexts);
			remainingTexts.AddRange(crossedTexts);
		}

		/// <summary>
		/// Вычесть внутренние и добавить пересекающие прямоугольник сегменты в списки вычтенных и оставшихся.
		/// </summary>
		/// <param name="pt1">Левая нижняя часть прямоугольника.</param>
		/// <param name="pt2">Правая верхняя часть прямоугольника.</param>
		/// <param name="subtractedSegments">Вычтенные сегменты.</param>
		/// <param name="remainingSegments">Оставшиеся сегменты.</param>
		private void SubtractInternalAndAddCrossingSegments(SmlPoint2D pt1, SmlPoint2D pt2, out List<SmlSegment2D> subtractedSegments, out List<SmlSegment2D> remainingSegments)
		{
			subtractedSegments = new List<SmlSegment2D>();
			remainingSegments = new List<SmlSegment2D>();
			List<SmlSegment2D> crossedSegments = new List<SmlSegment2D>();

			foreach (SmlSegment2D segment in SegmentList)
			{
				bool isStartPointInside = IsPointInsideRectangle(segment.StartPoint, pt1, pt2);
				bool isEndPointInside = IsPointInsideRectangle(segment.EndPoint, pt1, pt2);

				if (isStartPointInside && isEndPointInside)
					subtractedSegments.Add(segment);
				else if (isStartPointInside || isEndPointInside)
					crossedSegments.Add(segment);
				else
					remainingSegments.Add(segment);
			}

			subtractedSegments.AddRange(crossedSegments);
			remainingSegments.AddRange(crossedSegments);
		}

		private void SubtractInternalAndAddCrossingTexts(SmlPoint2D pt1, SmlPoint2D pt2, out List<Text> subtractedTexts, out List<Text> remainingTexts)
		{
			subtractedTexts = new List<Text>();
			remainingTexts = new List<Text>();

			foreach (Text text in TextList)
			{
				if (IsPointInsideRectangle(text.OutlineRect.BasePoint, pt1, pt2) || IsPointInsideRectangle(text.OutlineRect.OppositePoint, pt1, pt2))
					subtractedTexts.Add(text);
				else
					remainingTexts.Add(text);
			}
		}

		/// <summary>
		/// Вычесть внутренние и пересекающие прямоугольник сегменты.
		/// </summary>
		/// <param name="pt1">Левая нижняя часть прямоугольника.</param>
		/// <param name="pt2">Правая верхняя часть прямоугольника.</param>
		/// <param name="subtractedSegments">Вычтенные сегменты.</param>
		/// <param name="remainingSegments">Оставшиеся сегменты.</param>
		private void SubtractInternalAndCrossingSegments(SmlPoint2D pt1, SmlPoint2D pt2, out List<SmlSegment2D> subtractedSegments, out List<SmlSegment2D> remainingSegments)
		{
			subtractedSegments = new List<SmlSegment2D>();
			remainingSegments = new List<SmlSegment2D>();

			foreach (SmlSegment2D segment in SegmentList)
			{
				if (IsPointInsideRectangle(segment.StartPoint, pt1, pt2) || IsPointInsideRectangle(segment.EndPoint, pt1, pt2))
					subtractedSegments.Add(segment);
				else
					remainingSegments.Add(segment);
			}
		}

		#endregion

		#region Get methods

		/// <summary>
		/// Получение части чертежа, ограниченного прямоугольником.
		/// </summary>
		/// <param name="rect">Прямоугольник.</param>
		/// <param name="type">Опции</param>
		/// <returns></returns>
		public DrawingPart GetPartByRectangle(SmlRect2D rect, PartSelectionOptions type)
		{
			SmlBoundaryRect2D boundaryRect = rect.GetBoundaryRect();
			List<SmlSegment2D> segmList = new List<SmlSegment2D>();
			List<Text> textList = new List<Text>();
			if (type == PartSelectionOptions.InternalEntityOnly)
			{
				foreach (SmlSegment2D segment in SegmentList)
				{
					if (CheckSegmentAndRectanglePosition(segment, boundaryRect))
						continue;
					if (rect.IsInside(segment.StartPoint) && rect.IsInside(segment.EndPoint))
						segmList.Add(segment);
				}
				foreach (Text text in TextList)
				{
					if (CheckSegmentAndRectanglePosition(text.OutlineRect.BasePoint, text.OutlineRect.OppositePoint, boundaryRect))
						continue;
					if (rect.IsInside(text.OutlineRect.BasePoint) && rect.IsInside(text.OutlineRect.OppositePoint))
						textList.Add(text);
				}
			}
			else if (type == PartSelectionOptions.InternalAndCrossingEntity)
			{
				foreach (SmlSegment2D segment in SegmentList)
				{
					if (CheckSegmentAndRectanglePosition(segment, boundaryRect))
						continue;
					if (rect.IsInside(segment.StartPoint) || rect.IsInside(segment.EndPoint))
						segmList.Add(segment);
					else if (rect.HasIntersect(segment))
						segmList.Add(segment);
				}
				foreach (Text text in TextList)
				{
					if (CheckSegmentAndRectanglePosition(text.OutlineRect.BasePoint, text.OutlineRect.OppositePoint, boundaryRect))
						continue;
					if (rect.IsInside(text.OutlineRect.BasePoint) || rect.IsInside(text.OutlineRect.OppositePoint))
						textList.Add(text);
					else if (rect.HasIntersect(new SmlSegment2D(text.OutlineRect.BasePoint, text.OutlineRect.OppositePoint)))
						textList.Add(text);
				}
			}

			DrawingPart part = new DrawingPart(segmList, textList);
			return part;
		}

		/// <summary>
		/// Получение части чертежа, ограниченного прямоугольником.
		/// </summary>
		/// <param name="rect">Прямоугольник.</param>
		/// <param name="type">Опции</param>
		/// <returns></returns>
		public DrawingPart GetPartByRectangle(SmlBoundaryRect2D rect, PartSelectionOptions type)
		{
			List<SmlSegment2D> segmList = new List<SmlSegment2D>();
			List<Text> textList = new List<Text>();
			if (type == PartSelectionOptions.InternalEntityOnly)
			{
				foreach (SmlSegment2D segment in SegmentList)
				{
					if (IsPointInsideRectangle(segment.StartPoint, rect.BasePoint, rect.OppositePoint) && IsPointInsideRectangle(segment.EndPoint, rect.BasePoint, rect.OppositePoint))
						segmList.Add(segment);
				}
				foreach (Text text in TextList)
				{
					if (IsPointInsideRectangle(text.OutlineRect.BasePoint, rect.BasePoint, rect.OppositePoint) && IsPointInsideRectangle(text.OutlineRect.OppositePoint, rect.BasePoint, rect.OppositePoint))
						textList.Add(text);
				}
			}
			else if (type == PartSelectionOptions.InternalAndCrossingEntity)
			{
				foreach (SmlSegment2D segment in SegmentList)
				{
					if (IsPointInsideRectangle(segment.StartPoint, rect.BasePoint, rect.OppositePoint) || IsPointInsideRectangle(segment.EndPoint, rect.BasePoint, rect.OppositePoint))
						segmList.Add(segment);
					else if (rect.HasIntersect(segment))
						segmList.Add(segment);
				}
				foreach (Text text in TextList)
				{
					if (IsPointInsideRectangle(text.OutlineRect.BasePoint, rect.BasePoint, rect.OppositePoint) || IsPointInsideRectangle(text.OutlineRect.OppositePoint, rect.BasePoint, rect.OppositePoint))
						textList.Add(text);
					else if (rect.HasIntersect(new SmlSegment2D(text.OutlineRect.BasePoint, text.OutlineRect.OppositePoint)))
						textList.Add(text);
				}
			}

			DrawingPart part = new DrawingPart(segmList, textList);
			return part;
		}

		/// <summary>
		/// Получение части чертежа, ограниченного прямоугольником.
		/// </summary>
		/// <param name="pt1">Левая нижняя точка прямоугольника.</param>
		/// <param name="pt2">Правая верхняя точка прямоугольника.</param>
		/// <param name="type">Опции</param>
		/// <returns></returns>
		public DrawingPart GetPartByRectangle(SmlPoint2D pt1, SmlPoint2D pt2, PartSelectionOptions type)
		{
			return GetPartByRectangle(new SmlBoundaryRect2D(pt1, pt2), type);
		}

		/// <summary>
		/// </summary>
		/// <param name="point">Точка, которую необходимо проверить.</param>
		/// <param name="rectPt1">Левая нижняя точка прямоугольника.</param>
		/// <param name="rectPt2">Правая верхняя точка прямоугольника.</param>
		/// <returns>
		/// 	<c>true</c> если операции выполнена успешна, иначе <c>false</c>
		/// </returns>
		private bool IsPointInsideRectangle(SmlPoint2D point, SmlPoint2D rectPt1, SmlPoint2D rectPt2)
		{
			if (rectPt1.X > point.X) return false;
			if (rectPt2.X < point.X) return false;
			if (rectPt1.Y > point.Y) return false;
			if (rectPt2.Y < point.Y) return false;

			return true;
		}

		/// <summary>
		/// Находятся ли обе точки сегмента левее, 
		///						обе точки правее,
		///						обе точки выше,
		///	или обе точки ниже границ  окаймляющего прямоугольника. 
		/// </summary>
		/// <param name="s">Сегмент.</param>
		/// <param name="basePoint">Базовая точка окаймляющего прямоугольника.</param>
		/// <param name="oppositePoint">Противоположная точка окаймляющего прямоугольника.</param>
		/// <returns>
		/// 	<c>true</c> если операции выполнена успешна, иначе <c>false</c>
		/// </returns>
		private bool CheckSegmentAndRectanglePosition(SmlSegment2D s, SmlPoint2D basePoint, SmlPoint2D oppositePoint)
		{
			if (s.StartPoint.X > oppositePoint.X && s.EndPoint.X > oppositePoint.X)
				return true;

			if (s.StartPoint.X < basePoint.X && s.EndPoint.X < basePoint.X)
				return true;

			if (s.StartPoint.Y > oppositePoint.Y && s.EndPoint.Y > oppositePoint.Y)
				return true;

			if (s.StartPoint.Y < basePoint.Y && s.EndPoint.Y < basePoint.Y)
				return true;

			return false;
		}

		/// <summary>
		/// Находятся ли обе точки сегмента левее,
		/// обе точки правее,
		/// обе точки выше,
		/// или обе точки ниже границ  окаймляющего прямоугольника.
		/// </summary>
		/// <param name="s">Сегмент.</param>
		/// <param name="boundaryRect">Окаймляющий прямоугольник.</param>
		/// <returns>
		/// 	<c>true</c> если операции выполнена успешна, иначе <c>false</c>
		/// </returns>
		private bool CheckSegmentAndRectanglePosition(SmlSegment2D s, SmlBoundaryRect2D boundaryRect)
		{
			return CheckSegmentAndRectanglePosition(s, boundaryRect.BasePoint, boundaryRect.OppositePoint);
		}

		/// <summary>
		/// Находятся ли обе точки сегмента левее,
		/// обе точки правее,
		/// обе точки выше,
		/// или обе точки ниже границ  окаймляющего прямоугольника.
		/// </summary>
		/// <param name="segmPt1">Точка сегмента 1.</param>
		/// <param name="segmPt2">Точка сегмента 2.</param>
		/// <param name="basePoint">Базовая точка окаймляющего прямоугольника.</param>
		/// <param name="oppositePoint">Противоположная точка окаймляющего прямоугольника.</param>
		/// <returns>
		/// 	<c>true</c> если операции выполнена успешна, иначе <c>false</c>
		/// </returns>
		private bool CheckSegmentAndRectanglePosition(SmlPoint2D segmPt1, SmlPoint2D segmPt2, SmlPoint2D basePoint, SmlPoint2D oppositePoint)
		{
			return CheckSegmentAndRectanglePosition(new SmlSegment2D(segmPt1, segmPt2), basePoint, oppositePoint);
		}

		/// <summary>
		/// Находятся ли обе точки сегмента левее,
		/// обе точки правее,
		/// обе точки выше,
		/// или обе точки ниже границ  окаймляющего прямоугольника.
		/// </summary>
		/// <param name="segmPt1">Точка сегмента 1.</param>
		/// <param name="segmPt2">Точка сегмента 2.</param>
		/// <param name="boundaryRect">Окаймляющий прямоугольник.</param>
		/// <returns>
		/// 	<c>true</c> если операции выполнена успешна, иначе <c>false</c>
		/// </returns>
		private bool CheckSegmentAndRectanglePosition(SmlPoint2D segmPt1, SmlPoint2D segmPt2, SmlBoundaryRect2D boundaryRect)
		{
			return CheckSegmentAndRectanglePosition(new SmlSegment2D(segmPt1, segmPt2), boundaryRect);
		}

		#endregion

		#region Merge methods

		public void Add(DrawingPart part)
		{
			_segmentList.AddRange(part._segmentList);
			_textList.AddRange(part._textList);
		}

		#endregion
	}
}
