// Copyright (c) 2012 Виталий Сайдин, Ярослав Горин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Yaroslav Gorin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Drawing.Texts.TextParsers
{
	public class MTextParser : ITextParser
	{
		public string Parse(string stext)
		{
			try
			{
				string mtext = stext;
				/*1. ����� ������������ ��������*/
				mtext = mtext.Replace("%%D", "����");
				mtext = mtext.Replace("%%P", "+/-");
				mtext = mtext.Replace("%%C", "d");

				/*2. ������� �� �������� ������ ������*/
				mtext = mtext.Replace("\\P", "");

				/*������� �� �������������� ������*/
				mtext = mtext.Replace("\\L", "");
				mtext = mtext.Replace("\\l", "");
				mtext = mtext.Replace("\\O", "");
				mtext = mtext.Replace("\\o", "");

				/*������� �� ������*/
				mtext = mtext.Replace("{", "");
				mtext = mtext.Replace("}", "");

				/*3. ����� ��������� �������� \\ch...; , ��� ch - �� ';' �� '{' �� '}' �� '\' */
				/*3.1 */

				const string startStr = "\\";
				const string endStr = ";";
				int lastStart = 0;
				while (mtext.IndexOf(startStr, lastStart) != -1 && mtext.Length > lastStart)
				{
					lastStart = mtext.IndexOf(startStr, lastStart);

					/*������� ������, ��������� �� */
					char ch = mtext[lastStart + 1];
					if (ch != ';' && ch != '{' && ch != '}' && ch != '\\')
					{
						/*�������� � ������� ���������� ����������� ������ ';'*/
						int indexEnd = mtext.IndexOf(endStr, lastStart);
						if (indexEnd != -1)
						{
							int countDelete = indexEnd - lastStart + 1;
							/*��������, ��� ����� ���� ��� ������*/
							string subS = mtext.Substring(lastStart + 1, countDelete - 1);
							if (!subS.Contains("\\"))
							{
								/*� ������ ����� ������ ��������� ���������*/
								mtext = mtext.Remove(lastStart, countDelete);
							}
							else
							{
								lastStart++;
							}
						}
						else
						{
							lastStart += startStr.Length;
						}
					}
					else
					{
						lastStart++;
					}
				}

				/*4. ������������� �� ������� ������*/
				mtext = mtext.Replace("\\\\", "\\");
				mtext = mtext.Trim();

				while (mtext.Contains("  "))
					mtext = mtext.Replace("  ", " ");

				return mtext;
			}
			catch (Exception exc)
			{
				throw new Exception("������ �������� multitext " + stext, exc);
			}
		}

		/// <summary>
		/// ����� �� ������� ������
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public bool NeedParse(string str)
		{
			if (str.Contains("\\")) return true;
			return false;
		}
	}
}
