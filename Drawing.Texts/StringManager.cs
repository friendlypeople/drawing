// Copyright (c) 2012 Виталий Сайдин, Ярослав Горин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Yaroslav Gorin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

namespace Drawing.Texts
{
	/// <summary>
	/// �������������� ������ � ������������ � ��� ������������� �� �������.
	/// ����� ������� ������ ����.
	/// </summary>
	public class StringManager
	{
		/// <summary>
		/// ������ "������" ������
		/// </summary>
		private readonly List<Text> _text = new List<Text>();

		/// <summary>
		/// ������ �����
		/// </summary>
		private readonly List<List<Text>> _stringArray = new List<List<Text>>();

		public StringManager() { }

		public StringManager(IEnumerable<Text> text) 
		{
			_text = new List<Text>(text);
		}

		/// <summary>
		/// ���������� �����, ������������� � �����. � ������������� �� �������
		/// � ����������� � ���� ������
		/// </summary>
		/// <returns></returns>
		public string GetConcatString()
		{
			SplitToRows();
			SortRows();
			SortTextRows();
			return MergeStrings();
		}

		/// <summary>
		/// ����������� ����� �� �������
		/// </summary>
		private void SplitToRows()
		{
			/*��������� 1-� ������ � ������ �����*/
			_stringArray.Clear();
			_text.Sort(new VerticalTextComparator());

			foreach (Text t in _text)
			{
				bool isNewRow = true;
				foreach (List<Text> row in _stringArray)
				{
					Text rowText = row[0];

					double cy1 = t.OutlineRect.BasePoint.Y;
					double cy2 = rowText.OutlineRect.BasePoint.Y;

					// ����� �� ������� ����. 
					if (Math.Abs(cy2 - cy1) < rowText.OutlineRect.Height / 2)
					{
						isNewRow = false;
						row.Add(t);
						break;
					}
				}
				if (isNewRow)
				{
					List<Text> newRow = new List<Text>();
					newRow.Add(t);
					_stringArray.Add(newRow);
				}
			}
		}
		/// <summary>
		/// ����������� ������ �� ������
		/// </summary>
		private void SortRows()
		{
			VerticalStringComparator comp = new VerticalStringComparator();
			_stringArray.Sort(comp);
		}

		

		/// <summary>
		/// ������� ������ � ����.
		/// </summary>
		private string MergeStrings()
		{
			string concString = string.Empty;
			foreach (List<Text> row in _stringArray) 
			{
				foreach (Text t in row) 
				{
					concString += t.TextString + " "; 
				}
			}
			if (concString.EndsWith(" "))
				concString = concString.Remove(concString.Length - 1, 1);
			return concString;
		}

		/// <summary>
		/// ����������� ����� � ������� �� �����������
		/// </summary>
		private void SortTextRows() 
		{
			HorizontalStringComparator comp = new HorizontalStringComparator();
			foreach (List<Text> row in _stringArray)
			{
				row.Sort(comp);
			}
		}

		private sealed class HorizontalStringComparator : IComparer<Text>
		{
			public int Compare(Text x, Text y)
			{
				return x.OutlineRect.BasePoint.X.CompareTo(y.OutlineRect.BasePoint.X);
			}
		}

		private sealed class VerticalStringComparator : IComparer<List<Text>>
		{
			public int Compare(List<Text> x, List<Text> y)
			{
				Text tX = x[0];
				Text tY = y[0];
				return tY.OutlineRect.BasePoint.Y.CompareTo(tX.OutlineRect.BasePoint.Y);
			}
		}

		private sealed class VerticalTextComparator : IComparer<Text>
		{
			public int Compare(Text x, Text y)
			{
				return y.OutlineRect.BasePoint.Y.CompareTo(x.OutlineRect.BasePoint.Y);
			}
		}
	}
}
