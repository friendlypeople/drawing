// Copyright (c) 2012 Виталий Сайдин, Ярослав Горин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Yaroslav Gorin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Drawing.Texts.TextParsers;
using Drawing.Types;
using Sml.Geometry.Data;

namespace Drawing.Texts
{
	/// <summary>
	/// ������������� ������
	/// </summary>
	public class Text
	{
		private object					_tag;
		private string					_textString		= String.Empty;
		private string					_rawString		= String.Empty;
		private string					_textStyle		= String.Empty;
		private int						_size;
		private double					_rotation;
	    private long                    _handle;
		private Alignment				_alignment		= Alignment.CenterCenter;
		private SmlRect2D				_outlineRect	= new SmlRect2D();
		private SmlPoint2D				_charPoint		= new SmlPoint2D();
		private readonly ITextParser	_parser			= new MTextParser();

        public Text() { }

		public Text(string text)
		{
			_rawString = text;
		}

		public Text(string text, ITextParser parser)
		{
			_rawString = text;
			_parser = parser;
		}

		public string TextString
		{
			get
			{
				if (string.IsNullOrEmpty(_textString))
					_textString = _parser.NeedParse(_rawString) ? _parser.Parse(_rawString) : _rawString;
				return _textString;
			}
			set
			{
				if (!_rawString.Equals(value))
					_textString = string.Empty;
				_rawString = value;
			}
		}

		public override string ToString()
		{
			return TextString;
		}

		public int Size
		{
			get { return _size; }
			set { _size = value; }
		}

		public double Rotation
		{
			get { return _rotation; }
			set { _rotation = value; }
		}

		public string TextStyle
		{
			get { return _textStyle; }
			set { _textStyle = value; }
		}

		public SmlRect2D OutlineRect
		{
			get { return _outlineRect; }
			set { _outlineRect = value; }
		}

		public bool IsValid 
		{
			get { return !string.IsNullOrEmpty(TextString); }
		}

		public object Tag
		{
			get { return _tag; }
			set { _tag = value; }
		}

		public Alignment Alignment
		{
			get { return _alignment; }
			set { _alignment = value; }
		}

		public SmlPoint2D CharPoint
		{
			get { return _charPoint; }
			set { _charPoint = value; }
		}

        /// <summary>
        /// �������� ��� ������ Handle
        /// </summary>
        /// <value>
        /// Handle
        /// </value>
	    public long Handle
	    {
	        get { return _handle; }
	        set { _handle = value; }
	    }
	}
}
